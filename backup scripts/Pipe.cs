using UnityEngine;
using System.Collections;

public class Pipe : MonoBehaviour {
    public float moveSpeed;
	public GameObject pipe;
	private bool spawned = false, scored = false;
	public static bool gameover = false;
    public static bool gameStarted = false;
	// Use this for initialization
	void Start () {
	 
	}
	
	// Update is called once per frame
	void Update () 
    {
		if (!gameover && gameStarted)
		{
            transform.Translate(Vector3.left * moveSpeed * Time.deltaTime);
            float xPosistion = transform.position.x;
            if (xPosistion <= -1.6 && !scored)
            {
                scored = true;
                Scoring.setScore();
            }
		if (xPosistion < -1.50 && !spawned) 
            {
                spawned = true;
                createPipe();
			}

		if (xPosistion < -3.2) {
			//Object.Destroy(this.gameObject);
			Destroy(this.gameObject);
				}
		}
        

	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if(col.gameObject.tag == "Bird" && !gameover)
		{
			gameover = true;
			//bird.rigidbody2D.gravityScale = 1000;
			Debug.Log("collision detected");
			//BirdController.jumpForce = 0;
		}

	}

	void createPipe()
	{
		float yPosition = Random.Range(1.00f,5.34f);
		Vector3 spawnPosition = new Vector3(3.129f,yPosition,0);
		Instantiate(pipe,spawnPosition,Quaternion.identity);
	}
}
