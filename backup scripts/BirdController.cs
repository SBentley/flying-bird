using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {
    public GUIText guitext;
    int score;
	public float moveSpeed;
	private Vector3 moveDirection;
	public float jumpForce;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (!Pipe.gameover)
		{

		    Vector3 currentPosition = transform.position;
		    
            if (Input.GetButtonDown("Jump") && !Pauser.isPaused) 
            {
                Pipe.gameStarted = true;
                rigidbody2D.gravityScale = 2.5f;
                rigidbody2D.velocity = Vector2.zero;
			    rigidbody2D.AddForce(new Vector2(0f, jumpForce));
			}		
		}

		//Vector3 target = moveDirection * moveSpeed + currentPosition;
		//transform.position = Vector3.Lerp(currentPosition,target,Time.deltaTime);
		//float targetAngle = Mathf.Atan2(moveDirection.y,moveDirection.x) * Mathf.Rad2Deg;
		//transform.rotation = Quaternion.Slerp(transform.rotation,Quaternion.Euler(0,0,targetAngle),turnSpeed*Time.deltaT
	}

    void setScore()
    {
        score = (int)guitext.Text;
        score++;
        guitext.text = score.ToString();
    }
}
