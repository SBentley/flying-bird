﻿using UnityEngine;
using System.Collections;

public class GameOver : MonoBehaviour {

    public Texture2D texture;
    public GUISkin skin;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if (Pipe.gameover) 
        {
            Pipe.gameover = false;
            Pipe.gameStarted = false;
            Application.LoadLevel("menu");
        }
	}

    void showGUI()
    {
        if ( GUI.Button(new Rect(Screen.width / 2 - 120,Screen.height  / 2 - 100,texture.width,texture.height),texture)) 
        {
            Application.LoadLevel("menu");
            Pipe.gameover = false;
            Pipe.gameStarted = false;
        }

    }
}
