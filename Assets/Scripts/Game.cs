﻿using UnityEngine;
using System.Collections;
using System.IO;

public class Game : MonoBehaviour {

    public Texture2D texture;
    public GUISkin skin;
    public static int score;
	// Use this for initialization
	void Start () {
        score = 0;

	}
	
	// Update is called once per frame
	void Update () {

	}

    public static void Over()
    {
        Pipe.gameover = false;
        Pipe.gameStarted = false;
        Application.LoadLevel("menu");
        if (score > getHighScore())
        {
            saveHighScore();
        }
        score = 0;
    }

    void showGUI()//Currently not used
    {
        if ( GUI.Button(new Rect(Screen.width / 2 - 120,Screen.height  / 2 - 100,texture.width,texture.height),texture)) 
        {
            Application.LoadLevel("menu");
            Pipe.gameover = false;
            Pipe.gameStarted = false;
        }

    }
    public static int getHighScore()
    {
        StreamReader sr = new StreamReader("HighScore.txt");
        int currentHighScore = int.Parse(sr.ReadLine());
        sr.Dispose();
        return currentHighScore;
    }

    static void saveHighScore()
    {
        StreamWriter sw = new StreamWriter("HighScore.txt");
        sw.WriteLine(score);
        sw.Dispose();
    }
}