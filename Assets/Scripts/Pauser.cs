﻿using UnityEngine;
using System.Collections;

public class Pauser : MonoBehaviour {
    
    public GUISkin skin;
	public Texture2D pauseButtonTexture;
	public static bool isPaused = false;
	// Update is called once per frame
	void OnGUI()
	{
        GUI.skin = skin;
		if ( GUI.Button(new Rect(10,10,128,128),pauseButtonTexture)) 
        {
            //( GUI.Button(new Rect(10,10,pauseButtonTexture.width,pauseButtonTexture.height),pauseButtonTexture)) 
			if (isPaused)
			{
				Time.timeScale = 1;
				Debug.Log("time scale = 1");
			}
			else 
			{
				Time.timeScale = 0;
				Debug.Log("time scale = 0");
			}
			isPaused = !isPaused;
		}

	}
}
