using UnityEngine;
using System.Collections;

public class BirdController : MonoBehaviour {
	public float moveSpeed;
	private Vector3 moveDirection;
	public float jumpForce;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (!Pipe.gameover)
		{
            checkHeight();
		    Vector3 currentPosition = transform.position;
		    
            if (Input.GetButtonDown("Jump") && !Pauser.isPaused) 
            {
                Pipe.gameStarted = true;
                rigidbody2D.gravityScale = 2.5f;
                rigidbody2D.velocity = Vector2.zero;
			    rigidbody2D.AddForce(new Vector2(0f, jumpForce));
			}		
		}
       
	}

    void checkHeight()
    {
        float yPosition = transform.position.y;

        if (yPosition < -3.15)
        {
            Game.Over();
        }

        if (yPosition > 4.70f)
        {
            transform.position = new Vector2(-1.56f,4.7f);
        }

    }

}
